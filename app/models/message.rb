require 'redis'
require 'json'

class Message < ApplicationRecord
  belongs_to :user

  after_commit :notify_new_message, on: :create
  after_destroy :notify_destruction

  private
  def notify_new_message
    # Message.connection.execute "NOTIFY messages, '#{self.id}'"
    $redis.publish 'new_message', self.id
  end

  private
  def notify_destruction
    if Message.count <= 0
      # Message.connection.execute "NOTIFY messages_destroyed"
      $redis.publish 'message_reset', ''
    end
  end

  class << self
    def on_change
      thread = Thread.new do
        Message.connection.execute 'LISTEN messages'
        Message.connection.execute 'LISTEN messages_destroyed'

        loop do
          Message.connection.raw_connection.wait_for_notify do |event, pid, message|
            yield event, message
          end
        end
      end

      thread.join
    ensure
      Thread.kill thread
      Message.connection.execute 'UNLISTEN messages'
      Message.connection.execute 'UNLISTEN messages_destroyed'
    end
  end
end
