class PlainController < ApplicationController
  include ActionController::Live

  def index
    response.headers['Content-Type'] = 'text/event-stream'
    sse = SSE.new(response.stream)

    begin
      sse.write '', retry: '60000'

      60.times do |i|
        sse.write Time.now.strftime('%c'), event: 'time', id: i

        if Time.now.strftime('%S') == '00'
          sse.write '', event: 'full_minute'
        end

        sleep 1
      end
    ensure
      sse.close
    end

    render body: nil
  end
end
