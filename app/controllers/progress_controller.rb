class ProgressController < ApplicationController
  include ActionController::Live

  def index
  end

  def progress
    response.headers['Content-Type'] = 'text/event-stream'
    sse = SSE.new(response.stream)

    1000.times do |i|
      sse.write (i + 1) / 10.0, event: 'progress'
      sleep 0.015
    end

    sse.write '', event: 'stop'
  rescue ClientDisconnected
    logger.info 'Client disconnected'
  ensure
    sse.close
  end
end
