submit = false;

$(document).on('DOMContentLoaded', function () {
  if (!Notification) {
    console.info('Desktop notifications not available in your browser. Try Chromium.');
    return;
  }

  if (Notification.permission !== "granted")
    Notification.requestPermission();
});

$(document).ready(function(e) {
  if ($('.chat').length > 0)
    scrollBottom();

  $('textarea').on('change keyup keydown paste', function() {
    if ($(this).val() && !submit) {
      $('button.send-message').prop('disabled', false);
    } else {
      $('button.send-message').prop('disabled', true);
    }
  }).on('keypress', function(e) {
    if (e.keyCode == 13 && !window.event.shiftKey) {
      if ($(this).val()) {
        $('.chat-form').submit();
      }
      return false
    }
  });

  $('.chat-form').submit(function(e) {
    submitForm($(this));
    return false;
  });

  $('.reset-chat').click(function() {
    resetChat($(this));
  });

  if ($('.chat').length > 0)
    keepChatUpdated();
});

function submitForm($form) {
  submit = true;
  $form.find('button').prop('disabled', true);
  var data = $form.serialize();
  $form.find('textarea').prop('disabled', true);

  $.ajax({
    url: $form.attr('action'),
    method: $form.attr('method'),
    data: data
  }).done(function() {
    $form.find('textarea').val('');
    $form.find('button').prop('disabled', false);
    $form.find('textarea').prop('disabled', false);
    $form.find('textarea').focus();
    submit = false;
    $('textarea').trigger('change');
  });
}

function keepChatUpdated() {
  var path = $('.update-url').val();
  var $chat = $('.chat');

  for (var i = 0; i < 1; i++) {
    var sse = $.SSE(path, {
      onMessage: function(e) {
        scrollHeight = $chat[0].scrollHeight
        data = JSON.parse(e.data);
        $chat.append(data.html);

        if (scrollHeight - $chat.scrollTop() == $chat.outerHeight()) {
          scrollBottom();
        }

        if (data.user != data.current_user)
          notifyMe(data.content, data.user);
      },
      onError: function() {
        console.log('there has been an error');
      },
      events: {
        message_reset: function() {
          $('.chat').children().remove();
        }
      }
    });

    sse.start();
  }
}

function scrollBottom() {
  $('.chat').scrollTop($('.chat')[0].scrollHeight);
}

function resetChat($button) {
  $button.prop('disabled', true);

  $.ajax({
    url: $button.data('url'),
    method: 'delete'
  }).done(function() {
    // $('.chat').children().remove();
    $button.prop('disabled', false)
  });
}

function notifyMe(content, user) {
  if (Notification.permission !== "granted")
    Notification.requestPermission();
  else {
    var notification = new Notification('New Message from ' + user, {
      body: content
    });

  }
}
