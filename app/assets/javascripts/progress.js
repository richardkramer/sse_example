$(document).ready(function() {
  $('button.progress-vanilla').click(function() {
    progress($(this).data('progress'), $(this).data('path'));
  })

  $('button.progress-jquery').click(function() {
    progress_jquery($(this).data('progress'), $(this).data('path'));
  })
});

function progress(id, path) {
  var $progress = $('.progress[data-id='+id+']');
  var $button = $('button[data-progress='+id+']');
  $button.prop('disabled', true);

  // new SSE connection
  var sse = new EventSource(path);

  // listener to message with custom event (progress)
  sse.addEventListener('progress', function(e) {
    updateProgress($progress, e.data);
  });

  // listener to all messages without custom event
  sse.onmessage = function(e) {
    console.log(e.data)
  }

  // close connection when server sends stop event
  // disabled for presentation
  // sse.addEventListener('stop', function() {
  //   sse.close()
  //   $button.prop('disabled', false);
  // });
}

function progress_jquery(id, path) {
  var $progress = $('.progress[data-id='+id+']');
  var $button = $('button[data-progress='+id+']');
  $button.prop('disabled', true);

  var sse = $.SSE(path, {
    onMessage: function(e) {
      console.log(e.data);
    },
    events: {
      progress: function(e) {
        updateProgress($progress, e.data);
      },
      stop: function() {
        sse.stop();
      }
    },
    onEnd: function() {
      $button.prop('disabled', false);
    }
  });

  sse.start();
}

function updateProgress($progress, p) {
  $progress.find('.progress-bar').css('right', (100 - p) + '%')
  $progress.find('.percent').html(Math.floor(p) + '%');
}
