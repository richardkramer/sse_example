Rails.application.routes.draw do
  get 'plain_sse', to: 'plain#index'
  get 'progress', to: 'progress#index'
  get 'progress_state', to: 'progress#progress'

  get 'chat', to: 'chat#index'
  post 'chat/send_message/:user', to: 'chat#send_message', as: 'send_message'
  get 'chat/update', to: 'chat#update', as: 'update_chat'
  delete 'chat/reset', to: 'chat#reset', as: 'reset_chat'

  root 'home#index'
end
