$redis = Redis.new

heartbeat_thread = Thread.new do
  loop do
    $redis.publish("heartbeat","thump")
    sleep 1.seconds
  end
end

at_exit do
  heartbeat_thread.kill
  $redis.quit
end
